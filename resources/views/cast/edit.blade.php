@extends('layout.master')

  @section('title')
<<<<<<< HEAD
    Tambah Data Pemain
=======
    Tambah Data Cast
>>>>>>> f6f271792fd66131e9b63e5f61ec3ade537c50e5
  @endsection

  @section('content')
    <div>
        <form action="/cast/{{$cast->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Nama</label>
                <input type="text" class="form-control" value="{{$cast->nama}}" name="nama" id="nama" placeholder="Masukkan Nama Pemain">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Umur</label>
                <input type="text" class="form-control" value="{{$cast->umur}}" name="umur" id="umur" placeholder="Masukkan Umur Pemain">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Biodata</label>
                <input type="text" class="form-control" value="{{$cast->bio}}" name="bio" id="bio" placeholder="Masukkan Biodata Pemain">
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
  @endsection