@extends('layout.master')

  @section('title')
    Sign Up Form
  @endsection

  @section('subtitle')
    Buat Account Baru!
  @endsection

  @section('content')
    <form action="/welcome" method="POST">
      @csrf
      <label for="first_name">First name:</label> <br> 
      <br>
      <input id="first_name" type="text" name="first name" required> <br> 
      <br>
      <label for="last_name">Last name:</label> <br> 
      <br>
      <input id="last_name" type="text" name="last name" required> <br> 
      <br>
      <label for="gender">Gender:</label> <br>
      <br>
      <input id="gender" type="radio" name="gender">Male
      <br>
      <input id="gender" type="radio" name="gender">Female
      <br>
      <input id="gender" type="radio" name="gender">Other
      <br>
      <br>
      <label>Nationality:</label> <br>
      <br>
      <select name="nation">
          <option value="1">Indonesian</option>
          <option value="2">Singaporian</option>
          <option value="3">Malaysian</option>
          <option value="4">Other</option>
      </select> <br>
      <br>
      <label>Language Spoken:</label> <br>
      <br>
      <input type="checkbox" name="language_spoken" id="">Bahasa Indonesia
      <br>
      <input type="checkbox" name="language_spoken" id="">English
      <br>
      <input type="checkbox" name="language_spoken" id="">Other
      <br>
      <br>
      <label for="bio">Bio:</label> <br>
      <br>
      <textarea id="bio" name="bio" cols="40" rows="10"></textarea> <br>
      <input type="submit" value="Sign Up">
    </form>
  @endsection

    
