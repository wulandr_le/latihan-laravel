  @extends('layout.master')

  @section('title')
    Halaman Utama
  @endsection

  @section('subtitle')
    Halaman Utama
  @endsection

  @section('content')
    <h1>SanberBook</h1> <br>
    <h2>Social Media Developer Santai Berkualitas</h2> <br>
    <p>Belajar dan Berbagi agar hidup ini semakin santai berkualitas</p> <br>
    <h3>Benefit Join di SanberBook</h3> <br>
  
    <ul>
        <li>Mendapatkan motivasi dari sesama developer</li>
        <li>Sharing knowledge dari para mastah Sanber</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul> <br>

    <h3>Cara Bergabung ke SanberBook</h3> <br>

    <ol>
        <li>Mengunjungi Website ini</li>
        <li>Mendaftar di <a href="/register">Form Sign Up</a></li>
        <li>Selesai!</li>
    </ol>
  @endsection
    