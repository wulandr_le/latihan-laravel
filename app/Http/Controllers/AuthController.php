<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar(){
        return view('page.form');
    }

    public function submit(Request $request){
        $firstname = $request['first_name'];
        $lastname = $request['last_name'];
        return view('page.selamat', compact('firstname', 'lastname'));
    }
}
